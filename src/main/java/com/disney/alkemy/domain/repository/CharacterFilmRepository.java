package com.disney.alkemy.domain.repository;


import com.disney.alkemy.domain.CharacterFilm;

import java.util.List;
import java.util.Optional;

public interface CharacterFilmRepository {

    List<CharacterFilm> getAll();

    CharacterFilm save(CharacterFilm characterFilm);

    void delete(int characterFilmId);

    Optional<CharacterFilm> getCharacterFilm(int characterFilmId);
}
