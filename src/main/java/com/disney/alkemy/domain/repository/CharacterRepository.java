package com.disney.alkemy.domain.repository;

import com.disney.alkemy.domain.Character;
import com.disney.alkemy.domain.CharacterFilm;

import java.util.List;
import java.util.Optional;

public interface CharacterRepository {


    List<Character> getAll();

    Optional<Character> getCharacterId(int characterId);

    Character save(Character character);

    void delete(int characterId);

    List<Character> getCharacterByName(String name);

    List<Character> getCharacterByAge(int age);

    List<CharacterFilm> getCharacterByFilm(int idfilm);

}
