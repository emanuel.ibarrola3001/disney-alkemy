package com.disney.alkemy.domain.repository;

import com.disney.alkemy.domain.Film;

import java.util.List;
import java.util.Optional;

public interface FilmRepository {

    List<Film> getAll();

    Optional<Film> getFilm(int filmId);

    List<Film> MovieByName(String name);

    Film save(Film film);

    void delete(int filmId);

}
