package com.disney.alkemy.domain.repository;

import com.disney.alkemy.domain.Gender;

import java.util.List;
import java.util.Optional;

public interface GenderRepository {

    List<Gender> getAll();

    Gender save(Gender gender);

    void delete(int genderId);

    Optional<Gender> getGender(int genderId);
}
