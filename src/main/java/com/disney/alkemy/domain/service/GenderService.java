package com.disney.alkemy.domain.service;

import com.disney.alkemy.domain.Gender;
import com.disney.alkemy.domain.repository.GenderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GenderService {
    @Autowired
    private GenderRepository genderRepository;

    public List<Gender> getAll() {
        return genderRepository.getAll();
    }


    public Optional<Gender> getGender(int genderId) {
        return genderRepository.getGender(genderId);
    }

    public Gender save(Gender gender) {
        return genderRepository.save(gender);
    }

    public Boolean delete(int genderId) {
        return getGender(genderId).map(gender -> {
            genderRepository.delete(genderId);
            return true;
        }).orElse(false);
    }


}
