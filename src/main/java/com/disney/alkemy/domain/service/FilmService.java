package com.disney.alkemy.domain.service;

import com.disney.alkemy.domain.Film;
import com.disney.alkemy.domain.repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FilmService {
    @Autowired
    private FilmRepository filmRepository;


    public List<Film> getAll() {
        return filmRepository.getAll();
    }

    public Optional<Film> getFilm(int filmId) {
        return filmRepository.getFilm(filmId);
    }


    public Film save(Film film) {
        return filmRepository.save(film);
    }


    public List<Film> MovieByName(String name) {
        return filmRepository.MovieByName(name);
    }

    public Boolean delete(int filmId) {
        return getFilm(filmId).map(film -> {
            filmRepository.delete(filmId);
            return true;
        }).orElse(false);
    }


}
