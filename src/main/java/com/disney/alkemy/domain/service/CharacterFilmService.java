package com.disney.alkemy.domain.service;

import com.disney.alkemy.domain.CharacterFilm;
import com.disney.alkemy.domain.repository.CharacterFilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CharacterFilmService {
    @Autowired
    private CharacterFilmRepository characterFilmRepository;


    public List<CharacterFilm> getAll(){
        return characterFilmRepository.getAll();
    }

    public Optional<CharacterFilm> getCharacterFilm(int characterFilmId){
            return characterFilmRepository.getCharacterFilm(characterFilmId);
    }


    public CharacterFilm save(CharacterFilm characterFilm){
        return characterFilmRepository.save(characterFilm);
    }


    public Boolean delete(int characterFilmId){
        return getCharacterFilm(characterFilmId).map(characterFilm -> {
            characterFilmRepository.delete(characterFilmId);
            return  true;
        }).orElse(false);
    }
}
