package com.disney.alkemy.domain.service;

import com.disney.alkemy.domain.Character;
import com.disney.alkemy.domain.CharacterFilm;
import com.disney.alkemy.domain.repository.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CharacterService {

    @Autowired
    private CharacterRepository characterRepository;


    public List<Character> getAll(){
        return  characterRepository.getAll();
    }

    public Optional<Character> getCharacterId(int characterId){
        return  characterRepository.getCharacterId(characterId);
    }

    public Character save(Character character){
        return  characterRepository.save(character);
    }

    public Boolean delete(int characterId){
        return getCharacterId(characterId).map(characters -> {
            characterRepository.delete(characterId);
            return true;
        }).orElse(false);
    }

    public List<Character> getCharacterByName(String name){
        return characterRepository.getCharacterByName(name);
    }

    public List<Character> getCharacterByAge(int age){
        return characterRepository.getCharacterByAge(age);
    }

    public List<CharacterFilm> getCharacterByFilm(int idfilm){
        return characterRepository.getCharacterByFilm(idfilm);
    }

}
