package com.disney.alkemy.persistence;

import com.disney.alkemy.domain.Gender;
import com.disney.alkemy.domain.repository.GenderRepository;
import com.disney.alkemy.persistence.crud.GeneroCrudRepository;
import com.disney.alkemy.persistence.entity.Genero;
import com.disney.alkemy.persistence.mapper.GenderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class GeneroRepository implements GenderRepository {
    @Autowired
    private GeneroCrudRepository generoCrudRepository;

    @Autowired
    private GenderMapper mapper;

    @Override
    public List<Gender> getAll() {
        List<Genero> generos = (List<Genero>) generoCrudRepository.findAll();
        return mapper.toGenders(generos);
    }


    @Override
    public Optional<Gender> getGender(int genderId) {
        return generoCrudRepository.findById(genderId).map(genero -> mapper.toGender(genero));
    }


    @Override
    public Gender save(Gender gender) {
        Genero genero = mapper.toGenero(gender);
        return mapper.toGender(generoCrudRepository.save(genero));
    }

    @Override
    public void delete(int idGenero) {
        generoCrudRepository.deleteById(idGenero);
    }


}
