package com.disney.alkemy.persistence.mapper;

import com.disney.alkemy.domain.Film;
import com.disney.alkemy.persistence.entity.Pelicula;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring",uses = GenderMapper.class)
public interface FilmMapper {

    @Mappings({
            @Mapping(source = "idPelicula",target = "filmId"),
            @Mapping(source = "titulo",target = "title"),
            @Mapping(source = "img",target = "img"),
            @Mapping(source = "fecha",target = "date"),
            @Mapping(source = "calificacion",target = "qualification"),
            @Mapping(source = "genero",target = "gender"),

    })
    Film toFilm(Pelicula pelicula);
    List<Film> toFilms(List<Pelicula> peliculas);

    @InheritInverseConfiguration
    @Mapping(target = "personajes",ignore = true)
    Pelicula toPelicula(Film film);
}
