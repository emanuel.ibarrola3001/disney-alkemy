package com.disney.alkemy.persistence.mapper;

import com.disney.alkemy.domain.CharacterFilm;
import com.disney.alkemy.persistence.entity.PersonajePelicula;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring",uses = {FilmMapper.class ,CharacterFilmMapper.class}  )
public interface CharacterFilmMapper {


    @Mappings({
            @Mapping(source = "id",target = "id"),
            @Mapping(source = "personaje",target = "character"),
            @Mapping(source = "pelicula",target = "film")
    })
    CharacterFilm toCharacterFilm(PersonajePelicula personajePelicula);
    List<CharacterFilm> toCharacters(List<PersonajePelicula> personajePeliculas);


    @InheritInverseConfiguration
    PersonajePelicula toPersonajePelicula(CharacterFilm characterFilm);

}
