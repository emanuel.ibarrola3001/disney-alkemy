package com.disney.alkemy.persistence.mapper;

import com.disney.alkemy.domain.Gender;
import com.disney.alkemy.persistence.entity.Genero;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GenderMapper {

    @Mappings({
            @Mapping(source = "idGenero",target = "genderId"),
            @Mapping(source = "nombre",target = "name"),
            @Mapping(source = "img",target = "img"),
    })
    Gender toGender(Genero genero);
    List<Gender> toGenders(List<Genero> generos);

    @InheritInverseConfiguration
    @Mapping(target = "peliculas",ignore = true)
    Genero toGenero(Gender gender);



}
