package com.disney.alkemy.persistence.mapper;

import com.disney.alkemy.domain.Character;
import com.disney.alkemy.persistence.entity.Personaje;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring",uses = FilmMapper.class)
public interface CharacterMapper {

    @Mappings({
            @Mapping(source = "idPersonaje",target = "characterId"),
            @Mapping(source = "nombre",target = "name"),
            @Mapping(source = "img",target = "img"),
            @Mapping(source = "edad",target = "age"),
            @Mapping(source = "peso",target = "weight"),
            @Mapping(source = "historia",target = "history"),

    })
    Character toCharacter(Personaje personaje);
    List<Character> toCharacters(List<Personaje> personajes);


    @InheritInverseConfiguration
    @Mapping(target = "peliculas",ignore = true)
    Personaje toPersonaje(Character character);

}
