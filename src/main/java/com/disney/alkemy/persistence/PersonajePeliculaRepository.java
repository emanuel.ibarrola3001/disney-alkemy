package com.disney.alkemy.persistence;

import com.disney.alkemy.domain.CharacterFilm;
import com.disney.alkemy.domain.repository.CharacterFilmRepository;
import com.disney.alkemy.persistence.crud.PersonajePeliculaCrudRepository;
import com.disney.alkemy.persistence.entity.PersonajePelicula;
import com.disney.alkemy.persistence.mapper.CharacterFilmMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PersonajePeliculaRepository implements CharacterFilmRepository {

    @Autowired
    private PersonajePeliculaCrudRepository personajePeliculaCrudRepository;

    @Autowired
    private CharacterFilmMapper mapper;


    @Override
    public List<CharacterFilm> getAll() {
        List<PersonajePelicula> personajePeliculas= (List<PersonajePelicula>) personajePeliculaCrudRepository.findAll();
        return mapper.toCharacters(personajePeliculas);
    }

    @Override
    public CharacterFilm save(CharacterFilm characterFilm) {
        PersonajePelicula personajePelicula= mapper.toPersonajePelicula(characterFilm);
        return  mapper.toCharacterFilm(personajePeliculaCrudRepository.save(personajePelicula));
    }

    @Override
    public void delete(int characterFilmId) {
        personajePeliculaCrudRepository.deleteById(characterFilmId);
    }

    @Override
    public Optional<CharacterFilm> getCharacterFilm(int characterFilmId) {
        return personajePeliculaCrudRepository.findById(characterFilmId)
                .map(personajePelicula -> mapper.toCharacterFilm(personajePelicula));
    }
}
