package com.disney.alkemy.persistence.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "personaje")
@Data
public class Personaje {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_personaje")
    private Integer idPersonaje;
    private String nombre;
    @Column(name = "imagen", columnDefinition = "BLOB")
    private byte[] img;
    private Integer edad;
    private Double peso;
    private String historia;

    @OneToMany(mappedBy = "personaje")
    private List<PersonajePelicula> peliculas;

    public Integer getIdPersonaje() {
        return idPersonaje;
    }

    public void setIdPersonaje(Integer idPersonaje) {
        this.idPersonaje = idPersonaje;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public byte[] getImg() {
        return img;
    }

    public void setImg(byte[] img) {
        this.img = img;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public String getHistoria() {
        return historia;
    }

    public void setHistoria(String historia) {
        this.historia = historia;
    }

    public List<PersonajePelicula> getPeliculas() {
        return peliculas;
    }

    public void setPeliculas(List<PersonajePelicula> peliculas) {
        this.peliculas = peliculas;
    }
}
