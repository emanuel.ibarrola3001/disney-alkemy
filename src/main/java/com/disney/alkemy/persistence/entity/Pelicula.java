package com.disney.alkemy.persistence.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "pelicula")
public class Pelicula {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pelicula")
    private Integer idPelicula;

    private String titulo;

    @Column(name = "imagen", columnDefinition = "BLOB")
    private byte[] img;

    @Column(name = "fecha_creacion")
    private Date fecha;

    private Integer Calificacion;
    @ManyToOne
    @JoinColumn(name ="id_genero", insertable = false, updatable = false)
    private Genero idGenero;

    @OneToMany(mappedBy = "pelicula")
    private List<PersonajePelicula> personajes;

    public Integer getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(Integer idPelicula) {
        this.idPelicula = idPelicula;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public byte[] getImg() {
        return img;
    }

    public void setImg(byte[] img) {
        this.img = img;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getCalificacion() {
        return Calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        Calificacion = calificacion;
    }

    public Genero getGenero() {
        return idGenero;
    }

    public void setGenero(Genero genero) {
        this.idGenero = genero;
    }

    public List<PersonajePelicula> getPersonajes() {
        return personajes;
    }

    public void setPersonajes(List<PersonajePelicula> personajes) {
        this.personajes = personajes;
    }

    public Genero getIdGenero() {
        return idGenero;
    }

    public void setIdGenero(Genero idGenero) {
        this.idGenero = idGenero;
    }
}
