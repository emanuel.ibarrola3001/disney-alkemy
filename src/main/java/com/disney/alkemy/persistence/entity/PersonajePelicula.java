package com.disney.alkemy.persistence.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "pelicula_personaje")
public class PersonajePelicula {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "personaje",insertable = false,updatable = false)
    private Personaje personaje;

    @ManyToOne
    @JoinColumn(name = "pelicula",insertable = false,updatable = false)
    private Pelicula pelicula;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Personaje getPersonaje() {
        return personaje;
    }

    public void setPersonaje(Personaje personaje) {
        this.personaje = personaje;
    }

    public Pelicula getPelicula() {
        return pelicula;
    }

    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }
}
