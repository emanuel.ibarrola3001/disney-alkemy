package com.disney.alkemy.persistence;

import com.disney.alkemy.domain.Film;
import com.disney.alkemy.domain.repository.FilmRepository;
import com.disney.alkemy.persistence.crud.PeliculaCrudRepository;
import com.disney.alkemy.persistence.entity.Pelicula;
import com.disney.alkemy.persistence.mapper.FilmMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PeliculaRepository implements FilmRepository {
    @Autowired
    private PeliculaCrudRepository peliculaCrudRepository;
    @Autowired
    private FilmMapper mapper;

    @Override
    public List<Film> getAll() {
        List<Pelicula> peliculas= (List<Pelicula>) peliculaCrudRepository.findAll();
        return mapper.toFilms(peliculas);
    }

    @Override
    public Optional<Film> getFilm(int filmId) {
        return peliculaCrudRepository.findById(filmId).map(pelicula -> mapper.toFilm(pelicula));
    }

    @Override
    public List<Film> MovieByName(String name) {
        List<Pelicula> peliculas= peliculaCrudRepository.buscarPeliculaPorNombre(name);
        return  mapper.toFilms(peliculas);
    }

    @Override
    public Film save(Film film) {
        Pelicula pelicula= mapper.toPelicula(film);
        return  mapper.toFilm(peliculaCrudRepository.save(pelicula));
    }

    @Override
    public void delete(int filmId) {
    peliculaCrudRepository.deleteById(filmId);
    }
}
