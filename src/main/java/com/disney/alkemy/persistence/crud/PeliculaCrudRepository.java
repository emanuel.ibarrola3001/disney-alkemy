package com.disney.alkemy.persistence.crud;

import com.disney.alkemy.persistence.entity.Pelicula;
import com.disney.alkemy.persistence.entity.PersonajePelicula;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PeliculaCrudRepository  extends CrudRepository<Pelicula,Integer> {


    @Query("SELECT P FROM Pelicula P where P.titulo like %:name%")
    List<Pelicula> buscarPeliculaPorNombre(@Param("name") String name);


}
