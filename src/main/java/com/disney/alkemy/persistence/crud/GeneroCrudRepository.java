package com.disney.alkemy.persistence.crud;

import com.disney.alkemy.persistence.entity.Genero;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface GeneroCrudRepository extends CrudRepository<Genero,Integer> {



    List<Genero> findByIdGeneroOrderByNombreAsc(int idGenero);

}
