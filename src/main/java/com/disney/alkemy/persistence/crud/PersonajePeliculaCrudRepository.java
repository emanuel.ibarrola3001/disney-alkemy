package com.disney.alkemy.persistence.crud;

import com.disney.alkemy.persistence.entity.PersonajePelicula;
import org.springframework.data.repository.CrudRepository;

public interface PersonajePeliculaCrudRepository extends CrudRepository<PersonajePelicula,Integer> {
}
