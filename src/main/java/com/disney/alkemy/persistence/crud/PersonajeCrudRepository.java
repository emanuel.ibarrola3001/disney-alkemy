package com.disney.alkemy.persistence.crud;

import com.disney.alkemy.persistence.entity.Personaje;
import com.disney.alkemy.persistence.entity.PersonajePelicula;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PersonajeCrudRepository extends CrudRepository<Personaje, Integer> {


    @Query("select p from Personaje p where p.nombre like %:name%")
    List<Personaje> buscarPersonajePorNombre(@Param("name") String name);

    @Query("select p from Personaje p where p.edad= :age")
    List<Personaje> buscarPersonajePorEdad(@Param("age") Integer age);

    @Query("SELECT P FROM PersonajePelicula P where P.pelicula.idPelicula = :idmovie")
    List<PersonajePelicula> buscarPersonajePorPelicula(@Param("idmovie") Integer idmovie);

}
