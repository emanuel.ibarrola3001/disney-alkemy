package com.disney.alkemy.persistence;

import com.disney.alkemy.domain.Character;
import com.disney.alkemy.domain.CharacterFilm;
import com.disney.alkemy.domain.repository.CharacterRepository;
import com.disney.alkemy.persistence.crud.PersonajeCrudRepository;
import com.disney.alkemy.persistence.entity.Personaje;
import com.disney.alkemy.persistence.entity.PersonajePelicula;
import com.disney.alkemy.persistence.mapper.CharacterFilmMapper;
import com.disney.alkemy.persistence.mapper.CharacterMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PersonajeRepository implements CharacterRepository {

    @Autowired
    private PersonajeCrudRepository personajeCrudRepository;
    @Autowired
    private CharacterMapper mapper;
    @Autowired
    private CharacterFilmMapper mapper2;


    @Override
    public List<Character> getAll() {
           List<Personaje> personajes= (List<Personaje>) personajeCrudRepository.findAll();
        return  mapper.toCharacters(personajes);
    }

    @Override
    public Optional<Character> getCharacterId(int characterId) {
        return personajeCrudRepository.findById(characterId).map(personaje -> mapper.toCharacter(personaje));
    }

    @Override
    public Character save(Character character) {
        Personaje personaje = mapper.toPersonaje(character);
        return  mapper.toCharacter(personajeCrudRepository.save(personaje));
    }

    @Override
    public void delete(int characterId) {
        personajeCrudRepository.deleteById(characterId);
    }

    @Override
    public List<Character> getCharacterByName(String name) {
        List<Personaje> personajes= personajeCrudRepository.buscarPersonajePorNombre(name);
        return  mapper.toCharacters(personajes);
    }

    @Override
    public List<Character> getCharacterByAge(int age) {
        List<Personaje> personajes=personajeCrudRepository.buscarPersonajePorEdad(age);
        return  mapper.toCharacters(personajes);
    }

    @Override
    public List<CharacterFilm> getCharacterByFilm(int idfilm) {
        List<PersonajePelicula> personajes=personajeCrudRepository.buscarPersonajePorPelicula(idfilm);
        return mapper2.toCharacters(personajes);
    }
}
