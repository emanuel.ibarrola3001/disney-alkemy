package com.disney.alkemy.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api(){
            return new Docket(DocumentationType.SWAGGER_2)
                    .select()
                    .apis(RequestHandlerSelectors.basePackage("com.disney.alkemy.web.controller"))
                    .paths(PathSelectors.any())
                    .build()
                    .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo(){
        return new ApiInfo(
                " Disney Alkemy Backend Api",
                "API para el manejo de personajes y peliculas de Disney",
                "V1",
                "Terminos del Servicio",
                new Contact("Emanuel Ibarrola","https://emanuelibarrola3001.github.io/Emanuel-Ibarrola/","emanuel.ibarrola3001@gmail.com"),
                "Licencia de Api","Api licencia url", Collections.emptyList()
        );
    }
}
