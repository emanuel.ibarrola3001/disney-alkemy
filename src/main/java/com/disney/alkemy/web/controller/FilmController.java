package com.disney.alkemy.web.controller;

import com.disney.alkemy.domain.Film;
import com.disney.alkemy.domain.service.FilmService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/movies")
@Api(value = "Acciones relacionadas con Peliculas", tags = "Acciones sobre Peliculas")
public class FilmController {

    @Autowired
    private FilmService filmService;

    @GetMapping("/all")
    @ApiOperation("Consultar todas las Peliculas")
    @ApiResponse(code = 200, message = "OK")
    public ResponseEntity<List<Film>> getAll() {
        return new ResponseEntity<>(filmService.getAll(), HttpStatus.OK);
    }


    @GetMapping("/{id}")
    @ApiOperation("Buscar pelicula por Id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Pelicula no encontrada")
    })
    public ResponseEntity<Film> getFilm(@ApiParam(value = "El id de pelicula", required = true, example = "1")
                                        @PathVariable("id") int filmId) {
        return filmService.getFilm(filmId)
                .map(film -> new ResponseEntity<>(film, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    @PostMapping("/save")
    @ApiOperation("Guardar Peliculas")
    @ApiResponse(code = 201, message = "OK")
    public ResponseEntity<Film> save(@RequestBody Film film) {
        return new ResponseEntity<>(filmService.save(film), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") int filmId) {
        if (filmService.delete(filmId)) {
            return new ResponseEntity(HttpStatus.OK);
        } else return new ResponseEntity(HttpStatus.NOT_FOUND);
    }


    @GetMapping("movieName/{name}")
    @ApiOperation("Buscar pelicula por nombre")
    @ApiResponse(code = 200, message = "OK")
    public ResponseEntity<List<Film>> MovieByName(@PathVariable("name") String name) {
        return new ResponseEntity<>(filmService.MovieByName(name), HttpStatus.OK);
    }




}
