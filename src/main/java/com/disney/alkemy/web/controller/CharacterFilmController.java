package com.disney.alkemy.web.controller;

import com.disney.alkemy.domain.CharacterFilm;
import com.disney.alkemy.domain.Gender;
import com.disney.alkemy.domain.service.CharacterFilmService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/characterFilm")
@Api(value = "Acciones relacionadas con el personaje pelicula", tags = "Acciones sobre PersonajePelicula")
public class CharacterFilmController {

    @Autowired
    private CharacterFilmService characterFilmService;


    @GetMapping("/all")
    @ApiOperation("Consultar todos los PersonajesPeliculas")
    @ApiResponse(code = 200, message = "OK")
    public ResponseEntity<List<CharacterFilm>> getAll() {

        return new ResponseEntity<>(characterFilmService.getAll(), HttpStatus.OK);
    }



    @GetMapping("/{id}")
    @ApiOperation("Buscar personajePelicula por Id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "personajePelicula no encontrado")
    })
    public ResponseEntity<CharacterFilm> getCharacterFilm(@ApiParam(value = "El id del personajePelicula", required = true, example = "1")
                                            @PathVariable("id") int characterFilmId) {
        return characterFilmService.getCharacterFilm(characterFilmId)
                .map(gender -> new ResponseEntity<>(gender, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/save")
    @ApiOperation("Guardar PersonajesPeliculas")
    @ApiResponse(code = 201, message = "OK")
    public ResponseEntity<CharacterFilm> save(@RequestBody CharacterFilm characterFilm) {

        return new ResponseEntity<>(characterFilmService.save(characterFilm), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("Eliminar un PersonajePelicula con un ID")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "PersonajePelicula no eliminado")
    })
    public ResponseEntity delete(@PathVariable("id") int PersonajePeliculaId) {
        if (characterFilmService.delete(PersonajePeliculaId)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
