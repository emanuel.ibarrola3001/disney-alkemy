package com.disney.alkemy.web.controller;

import com.disney.alkemy.domain.Character;
import com.disney.alkemy.domain.CharacterFilm;
import com.disney.alkemy.domain.service.CharacterService;
import com.disney.alkemy.persistence.entity.Personaje;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/characters")
@Api(value = "Acciones relacionadas con los Personajes", tags = "Acciones sobre Personajes")
public class CharacterController {

    @Autowired
    private CharacterService characterService;

    @GetMapping("/all")
    @ApiOperation("Consultar todos los Personajes")
    @ApiResponse(code = 200, message = "OK")
    public ResponseEntity<List<Character>> getAll() {
        return new ResponseEntity<>(characterService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation("Consultar peliculas por Id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Pelicula no encontrada")
    })
    public ResponseEntity<Character> getCharacterId(@ApiParam(value = "El id del personaje", required = true, example = "2")
                                                    @PathVariable("id") int characterId) {
        return characterService.getCharacterId(characterId)
                .map(character -> new ResponseEntity<>(character, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/save")
    @ApiOperation("Guardar Personajes")
    @ApiResponse(code = 200, message = "OK")
    public ResponseEntity<Character> save(@RequestBody Character character) {
        return new ResponseEntity<>(characterService.save(character), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") int characterId) {
        if (characterService.delete(characterId)) {
            return new ResponseEntity(HttpStatus.OK);
        } else return new ResponseEntity(HttpStatus.NOT_FOUND);

    }

    @GetMapping("/character/name/{name}")
    @ApiOperation("Buscar personajes por nombre")
    @ApiResponse(code = 200,message = "OK")
    public ResponseEntity<List<Character>> getCharacterByName(@PathVariable("name") String name) {
        return  new ResponseEntity<>(characterService.getCharacterByName(name),HttpStatus.OK);
    }

    @GetMapping("/character/age/{age}")
    @ApiOperation("Buscar personajes por edad")
    @ApiResponse(code = 200,message = "OK")
    public ResponseEntity<List<Character>> getCharacterByAge(@PathVariable("age") int age){
        return new ResponseEntity<>(characterService.getCharacterByAge(age),HttpStatus.OK);
    }


    @GetMapping("/character/film/{id}")
    @ApiOperation("Buscar personajes por edad")
    @ApiResponse(code = 200,message = "OK")
    public ResponseEntity<List<CharacterFilm>> getCharacterByFilm(@PathVariable("id") int idfilm){
        return new ResponseEntity<>(characterService.getCharacterByFilm(idfilm),HttpStatus.OK);
    }

}
