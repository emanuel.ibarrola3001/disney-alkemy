package com.disney.alkemy.web.controller;

import com.disney.alkemy.domain.Gender;
import com.disney.alkemy.domain.service.GenderService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/gender")
@Api(value = "Acciones relacionadas con el Genero", tags = "Acciones sobre Genero")
public class GenderController {

    @Autowired
    private GenderService genderService;

    @GetMapping("/all")
    @ApiOperation("Consultar todos los Generos")
    @ApiResponse(code = 200, message = "OK")
    public ResponseEntity<List<Gender>> getAll() {

        return new ResponseEntity<>(genderService.getAll(), HttpStatus.OK);
    }


    @GetMapping("/{id}")
    @ApiOperation("Buscar genero por Id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Genero no encontrado")
    })
    public ResponseEntity<Gender> getGender(@ApiParam(value = "El id de genero", required = true, example = "1")
                                            @PathVariable("id") int genderId) {
        return genderService.getGender(genderId)
                .map(gender -> new ResponseEntity<>(gender, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/save")
    @ApiOperation("Guardar Generos")
    @ApiResponse(code = 201, message = "OK")
    public ResponseEntity<Gender> save(@RequestBody Gender gender) {

        return new ResponseEntity<>(genderService.save(gender), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("Eliminar un genero con un ID")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Genero no eliminado")
    })
    public ResponseEntity delete(@PathVariable("id") int genderId) {
        if (genderService.delete(genderId)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }






}
